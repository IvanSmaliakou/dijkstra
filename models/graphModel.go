package models

type Vertex struct {
	Number int64
	Weight int64
	Marked bool
}
type Connection struct {
	Weight int64
	From   int64
	To     int64
}
