package service

import (
	"fmt"
	"graphLab/models"
	"math"
)

type Service struct{}

func NewConn(weight int64, from, to int64) models.Connection {
	return models.Connection{Weight: weight, From: from, To: to}
}

func NewVertex(number int64) models.Vertex {
	return models.Vertex{Number: number, Weight: 1<<63 - 10000}
}

func (s *Service) IterateOverGraph(vertexes []models.Vertex, conns []models.Connection) {
	cur := vertexes[0]
	for {
		min := int64(math.MaxInt64)
		currentConns := s.fromByNumber(cur.Number, conns)
		for _, c := range currentConns {
			before := vertexes[c.To-1].Weight
			after := cur.Weight + c.Weight
			if after <= before {
				vertexes[c.To-1].Weight = after
			}
		}
		vertexes[cur.Number-1].Marked = true
		if len(currentConns) == 0 {
			break
		}
		for _, v := range vertexes {
			if v.Marked {
				continue
			}
			if v.Weight < min {
				min = v.Weight
				cur = v
			}
		}
	}
	fmt.Println(vertexes)
}

func (_ *Service) fromByNumber(n int64, conns []models.Connection) []models.Connection {
	ret := make([]models.Connection, 0)
	for _, conn := range conns {
		if conn.From == n {
			ret = append(ret, conn)
		}
	}
	return ret
}
