package main

import (
	"graphLab/models"
	"graphLab/service"
)

func main() {
	v1 := service.NewVertex(1)
	v1.Weight = 0
	v2 := service.NewVertex(2)
	v3 := service.NewVertex(3)
	v4 := service.NewVertex(4)
	v5 := service.NewVertex(5)
	v6 := service.NewVertex(6)
	v7 := service.NewVertex(7)
	v8 := service.NewVertex(8)
	v9 := service.NewVertex(9)
	vs := []models.Vertex{v1, v2, v3, v4, v5, v6, v7, v8, v9}

	c12 := service.NewConn(4, 1, 2)
	c18 := service.NewConn(20, 1, 8)
	c16 := service.NewConn(12, 1, 6)
	c15 := service.NewConn(13, 1, 5)
	c25 := service.NewConn(8, 2, 5)
	c23 := service.NewConn(8, 2, 3)
	c34 := service.NewConn(10, 3, 4)
	c49 := service.NewConn(8, 4, 9)
	c35 := service.NewConn(8, 3, 5)
	c54 := service.NewConn(10, 5, 4)
	c57 := service.NewConn(12, 5, 7)
	c65 := service.NewConn(10, 6, 5)
	c79 := service.NewConn(4, 7, 9)
	c74 := service.NewConn(4, 7, 4)
	c89 := service.NewConn(16, 8, 9)
	c87 := service.NewConn(12, 8, 7)
	conns := []models.Connection{c12, c18, c16, c15, c25, c23, c34, c49, c35, c54, c57, c65, c79, c74, c89, c87}

	s := &service.Service{}
	s.IterateOverGraph(vs, conns)
}
